# Role Play Game

## Présentation du projet 

Role play game est un projet de jeu qui a pour but de mettre en scène un joueur qui doit chosir une classe de personnage entre Guerrier, Mage ou Archer.
Il devra tout au long du jeu faire des choix pour pouvoir finir le jeu, plusieurs fins sont possibles.


## Possibilités de l'application 

Le joueur peut choisir un pseudo qui sera sauvegardé, pendant toute la durée du jeu le joueur devra cliquer sur des boutons qui lui proposera des choix.


## Architecture du projet

Cette application est découpée en trois repo différents :
- Un côté front 
- [Un coté backend](https://github.com/Zarkrom/RP_Game_Back) 
- [Les tests selenium](https://gitlab.com/hugo38rodrigues/selenium_role_playe)


## Choix technologique 

Pour le front, l'application utilise React 18, axios 1.4 et react-router-dom 6.11.
L'application possède des tests unitaires coté front et pour ce faire, jest a été installé.


## Installer l'application 

Récupérez le front de l'application avec la commande suivante :
```
git clone git@gitlab.com:{nom_de_votre_compte}/role-playe-game.git
```

## Front End 
Sur l'application, lancez la commande suivante pour installer les dépendances :
```
npm i --force
```

Enfin pour lancer l'application, faites :
```
npm run dev
```

## Lancer les tests unitaires 
Pour lancer vos test unitaire il suffis de lancer la commande 
``` ecmascript 6
npx jest
```