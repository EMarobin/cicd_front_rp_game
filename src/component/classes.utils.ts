const REGEX_NAME_PLAYER = /^[a-zA-Z0-9]{1,10}$/;
export const checkNamePlayer = (name: string): boolean => {
    return REGEX_NAME_PLAYER.test(name);
};