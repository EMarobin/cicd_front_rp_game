import {FunctionComponent, useState} from 'react'
import {HeroClasses, HeroProps} from '../interface/classes'
import {checkNamePlayer} from "./classes.utils";

const Classes: FunctionComponent<HeroProps> = (props) => {
    const { archer, warrior, mage, handleSelectionHero } = props;
    const [isErrorName, setIsErrorName] = useState(false);
    const [playerName, setPlayerName] = useState('');


    const selectHero = (hero: string): void => {
        if (checkNamePlayer(playerName)) {
            handleSelectionHero(hero, playerName);
            setIsErrorName(false);
        } else {
            setIsErrorName(true);
        }
    };

    return (
        <div className="hero-classes">
            <h1>Choisissez votre classe</h1>
            <div className="heros-items">
                <div className='name-player'>
                    <label
                        form="namePlayer"
                    >
                        <strong>
                            Choisissez votre pseudo
                        </strong>
                    </label>
                    <input
                        type="text"
                        name="namePlayer"
                        id="namePlayer"
                        maxLength={10}
                        onChange={(e) => setPlayerName(e.target.value)}
                    />
                    {isErrorName &&
                        <div className='error-message'>
                            <p> Veuillez choisir un pseudo qui contient :</p>
                            <ul>
                                <ol>Des lettres</ol>
                                <ol>Des nombres</ol>
                            </ul>
                    </div>}
                </div>
                <div className="warrior" onClick={() => selectHero(warrior.class)}>
                    <h2>{warrior.class}</h2>
                    <ul>
                        <ol> HP: {warrior.stats.hp}</ol>
                        <ol>ATK: {warrior.stats.ATK}</ol>
                    </ul>
                </div>
                <div className="mage" onClick={() => selectHero(mage.class)}>
                    <h2>{mage.class}</h2>
                    <ul>
                        <ol> HP: {mage.stats.hp}</ol>
                        <ol>ATK: {mage.stats.ATK}</ol>
                    </ul>
                </div>
                <div className="archer" onClick={() => selectHero(archer.class)}>
                    <h2>{archer.class}</h2>
                    <ul>
                        <ol> HP: {archer.stats.hp}</ol>
                        <ol>ATK: {archer.stats.ATK}</ol>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Classes;