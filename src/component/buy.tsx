import {BuysStatistics} from '../interface/buys-statistics.js'
import {FunctionComponent} from "react";


interface BuysProps {
    items: BuysStatistics
    hero: string
    handleSelectedItem: (itemBuy:number) => void
}

const Buy: FunctionComponent<BuysProps> = (props) => {
    const {items, hero,handleSelectedItem} = props
    const {warrior, mage, archer} = items
     const isWarrior = hero === 'Warrior'
    const isMage = hero === 'Mage'
    const isArcher = hero === 'Archer'


    const handlePostItemBuy = (itemBuy: number ) => {
        handleSelectedItem(itemBuy)
    }


    return (
        <div className="list-stuff">
            <h2>Choissisez un bonus pour commencer la partie</h2>
            {isWarrior &&
                <div className="stuff-warrior">
                    <div className="item-heros" onClick={() => handlePostItemBuy(warrior.pts_item)}>
                        <h2>{warrior.name}</h2>
                        <p>+ {warrior.pts_item} points ATK</p>
                    </div>
                    <div className="pts-heros" onClick={() => handlePostItemBuy(warrior.pts)}>
                        <h2>Ajouter {warrior.pts} points à vos stats de base de votre personnage</h2>
                    </div>
                </div>
            }
            {isMage &&
                <div className="stuff-mage">
                    <div className="item-heros" onClick={() => handlePostItemBuy(mage.pts_item)}>
                        <h2>{mage.name}</h2>
                        <p>+ {mage.pts_item} points ATK</p>
                    </div>
                    <div className="pts-heros" onClick={() => handlePostItemBuy(mage.pts)}>
                        <h2>Ajouter {mage.pts} points à vos stats de base de votre personnage</h2>
                    </div>
                </div>
            }
            {isArcher &&
                <div className="stuff-archer">
                    <div className="item-heros" onClick={() => handlePostItemBuy(archer.pts_item)}>
                        <h2>{archer.name} </h2>
                        <p>+ {archer.pts_item} points ATK</p>
                    </div>
                    <div className="pts-heros" onClick={() => handlePostItemBuy(archer.pts)}>
                        <h2>Ajouter {archer.pts} points à vos stats de base de votre personnage</h2>

                    </div>
                </div>
            }
        </div>
    );
};

export default Buy;