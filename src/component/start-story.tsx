import {useState} from "react";

interface StartStoryProp {
    namePlayer: string
}

const StartStory = (props: StartStoryProp) => {
    const {namePlayer} = props
    const [isStartStory, setIsStartStory] = useState(true)
    const [isTwoParagraph, setIsTwoParagraph] = useState(false)
    const [isThreeParagraph, setIsThreeParagraph] = useState(false)
    const [isFourParagraph, setIsFourParagraph] = useState(false)
    const [isFiveParagraph, setIsFiveParagraph] = useState(false)
    const handleParagraphOne = (): void => {
        setIsStartStory(false)
        setIsTwoParagraph(true)
    }
    const handleParagraphTwo = (): void => {
        setIsTwoParagraph(false)
        setIsThreeParagraph(true)
    }
    const handleParagraphThree = (): void => {
        setIsThreeParagraph(false)
        setIsFourParagraph(true)
    }

    const handleParagraphFour = (): void => {
        setIsFourParagraph(false)
        setIsFiveParagraph(true)
    }
    return (
        <div className="story">
            {isStartStory &&
                <>
                    <p>Dans l'atmosphère enfumée de la vieille taverne, les flammes crépitantes du foyer dansaient
                        joyeusement, éclairant les visages fatigués des aventuriers en quête d'action et de fortune.
                        Le doux son des rires et des chants résonnait à travers les murs usés, créant une symphonie
                        chaleureuse qui contrastait avec la froideur de la nuit.
                    </p>
                    <button onClick={handleParagraphOne}> continuer</button>
                </>
            }
            {isTwoParagraph &&
                <>
                    <p>Au milieu de cette scène animée se tenait notre héros, un individu au regard vif et à la stature
                        solide, dont les prunelles brûlaient d'une soif d'aventure.

                        Son nom était encore inconnu, mais son destin allait bientôt s'écrire.

                        Assis à une table de bois usée, notre héros observait les convives de la taverne, scrutant
                        chaque conversation animée et chaque geste dissimulant peut-être une proposition alléchante.

                        C'est alors que son regard fut attiré par une figure mystérieuse, enveloppée d'un manteau
                        sombre, installée à l'ombre d'un coin reculé.

                        Intrigué, notre héros décida de s'approcher, luttant contre la vague d'excitation qui montait en
                        lui. Il s'assit en face du mystérieux personnage, l'étudiant de son regard perçant.</p>
                    <button onClick={handleParagraphTwo}>continue</button>
                </>
            }
            {isThreeParagraph &&
                <>
                    <p>"L'heure est venue pour toi de prouver ta valeur, <strong>{namePlayer}</strong>", chuchota le
                        mystérieux inconnu d'une
                        voix grave et rocailleuse. "Une quête périlleuse et riche en récompenses t'attend, mais seuls
                        les plus braves osent s'y aventurer."</p>
                    <button onClick={handleParagraphThree}>continue</button>
                </>
            }
            {isFourParagraph &&
                <>
                    <p>Notre héros écoutait attentivement, sentant son cœur battre plus fort à chaque mot prononcé. Le
                        mystérieux personnage lui dévoila l'existence d'une antique relique, cachée dans les profondeurs
                        d'un labyrinthe maudit. Cette relique détenait un pouvoir immense, susceptible de changer le
                        cours du destin.

                        Il quitta la taverne, le vent froid de la nuit caressant son visage, et s'engagea sur la voie
                        incertaine qui le mènerait vers le labyrinthe maudit. La quête de sa vie venait de commencer, et
                        il était déterminé à la réussir, peu importe les obstacles qui se dresseraient sur son chemin.
                    </p>
                    <button onClick={handleParagraphFour} >continue</button>
                </>
            }
        </div>
    );
};

export default StartStory;