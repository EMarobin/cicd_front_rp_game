import {checkNamePlayer} from "../../component/classes.utils";


describe('CheckName', () => {
    it('Should return true with name player ', () => {
        const playerName = "jhon41"
        expect(checkNamePlayer(playerName)).toBe(true)
    });

    it('Should return false with name player ', () => {
        const playerName = "jhonùù"
        expect(checkNamePlayer(playerName)).toBe(false)
    });

    // Ajoutez d'autres tests pour les différentes fonctionnalités de votre hook useClassesHook
});