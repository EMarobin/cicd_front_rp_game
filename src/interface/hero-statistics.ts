export interface HeroStatistics {
    warrior: {
        id: number,
        class:"Guerrier",
        name: string,
        stats: {
            hp: number,
            ATK: number
        },
        status: string
    },
    mage: {
        id: number,
        class:"Mage",
        name: string,
        stats: {
            hp: number,
            ATK: number
        },
        status: string
    },
    archer: {
        id: number,
        class: "Archer",
        name: string,
        stats: {
            hp: number,
            ATK: number
        },
        status: string
    }
}