
export interface HeroClasses {
    id: number,
    class: string,
    name: string,
    stats: {
        hp: number,
        ATK: number
    },
    status?: string
}

export interface HeroProps {
    warrior: HeroClasses
    archer: HeroClasses
    mage: HeroClasses
    handleSelectionHero: (hero: string, name: string) => void
}

