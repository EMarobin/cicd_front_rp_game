export interface BuysStatistics {

    warrior: {
        "id": 1,
        "name": "Épée de la mort",
        "pts_item": 2,
        "pts": 4
    },
    mage: {
        "id": 2,
        "class": "mage",
        "name": "Baton du puissant sorcier",
        "pts_item": 2,
        "pts": 4
    },
    archer: {
        "id": 3,
        "class": "archer",
        "name": "Arc de l'arbre divin",
        "pts_item": 2,
        "pts": 4
    }
}