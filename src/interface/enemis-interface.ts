export interface EnemisInterface {
    snake: {
        hp: number,
        ATK: number,
        poison: number
    }
    bandi: {
        hp: number,
        ATK: number
    }
}