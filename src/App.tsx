import {BrowserRouter, Route, Routes} from 'react-router-dom';

import Home from '../src/view/home.tsx'
import Game from "./view/game.tsx";

const App = () => {


    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/game" element={<Game/>}/>
                </Routes>
            </BrowserRouter>
        </>)
}

export default App