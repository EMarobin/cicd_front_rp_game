import {Link} from "react-router-dom";

const Home = () => {
    return (
        <div className="home">
            <h1>Bienvenue dans jumanji</h1>
            <button id="start-game"> <Link to={"/game"}> Play game </Link></button>
            
        </div>
    );
};

export default Home;