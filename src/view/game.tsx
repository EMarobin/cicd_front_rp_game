import {useEffect, useState} from 'react';
import axios from 'axios';
import Buy from "../component/buy"
import {HeroStatistics} from "../interface/hero-statistics"
import {EnemisInterface} from "../interface/enemis-interface"
import {BuysStatistics} from "../interface/buys-statistics"
import StartStory from "../component/start-story"
import {HeroClasses} from "../interface/classes"
import Classes from "../component/classes";

interface HeroData {
    hero: HeroStatistics,
}
interface EnnemyData {
    ennemy: EnemisInterface

}


const Game = () => {
    const [classesHero, setClassesHero] = useState<HeroData>();
    const [enemys, setEnemys] = useState<EnnemyData>();
    const [items, setItems] = useState<BuysStatistics>();
    const [isClasses, setIsClasses] = useState(true)
    const [isBuyItem, setIsBuyItem] = useState(false)
    const [isStory, setIsStory] = useState(false)
    const [namePlayer, setNamePlayer] = useState('')
    const [selectedHero, setSelectedHero] = useState('')

    useEffect(() => {
        const fetchClassesHero = async () => {
            try {
                const response = await axios.get('http://localhost:5000/hero/get')
                setClassesHero(response.data);
            } catch (error) {
                console.log(error);
            }
        }
        const fetchStuff = async () => {
            try {
                const response = await axios.get('http://localhost:5000/item/get')
                setItems(response.data);
            } catch (error) {
                console.log(error);
            }
        }
        const fetchEnnemy = async () => {
            try {
                const response = await axios.get('http://localhost:5000/enemy/all')
                setEnemys(response.data);
            } catch (error) {
                console.log(error);
            }
        }
        fetchStuff()
        fetchClassesHero()
        fetchEnnemy()

    }, []);

    if (!classesHero) {

        return <div>Loading...</div>;
    }
    const {hero}= classesHero
    // const {enemy}= enemys





    const postPersoChoice = (hero,namePlayer) => {
        axios.post('http://localhost:5000/hero/init', {
            name: namePlayer,
            class: hero
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    const postStuffChoice = (stuff) => {
        axios.post('http://localhost:5000/hero/init', {
            stuff: stuff
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    const getHero = ( hero: string, namePlayer:string): void => {
        postPersoChoice(hero, namePlayer)
        setIsClasses(false)
        setIsBuyItem(true)
        setNamePlayer(namePlayer)
        setSelectedHero(hero)
    }

    const selectedItem = (item: number): void => {
        postStuffChoice(item)
        setIsBuyItem(false)
        setIsStory(true)
    }

    return (
        <>
            {isClasses &&
                <Classes
                    mage={hero.mage}
                    archer={hero.archer}
                    warrior={hero.warrior}
                    handleSelectionHero={getHero}
                />
            }
            {isBuyItem &&
                <Buy
                    items={items}
                    hero={selectedHero}
                    handleSelectedItem={selectedItem}
                />
            }
            {isStory && <StartStory namePlayer={namePlayer}/>}
        </>
    )

}


export default Game;
